<?php

namespace D3JDigital\Addresses\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;
use OpenSourceDeveloper\Reaktr\Core\Response\PaginationResponse;
use D3JDigital\Addresses\Contracts\Services\AddressServiceInterface;
use D3JDigital\Addresses\Database\Eloquent\Repositories\AddressRepository;
use D3JDigital\Addresses\Response\Entities\AddressEntity;

class AddressService implements AddressServiceInterface
{
    public function __construct(
        protected AddressRepository $addressRepository,
    ) {}

    public function getAddresses(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection
    {
        $result = $this->addressRepository->all($relations, $columns, $paginate);
        if ($result instanceof PaginationResponse) {
            return new LengthAwarePaginator(collect($result->getRecords())->mapInto(AddressEntity::class), $result->getRecordCount(), $result->getRecordsPerPage(), $result->getCurrentPage());
        }
        return collect($result)->mapInto(AddressEntity::class);
    }

    public function getAddress(int $id, array $relations = [], array $columns = ['*']): ?Entity
    {
        $result = $this->addressRepository->find($id, $relations, $columns);
        return $result ? (new AddressEntity)->fill($result) : null;
    }

    public function createAddress(array $attributes): ?Entity
    {
        $result = $this->addressRepository->create($attributes);
        return $result ? (new AddressEntity)->fill($result) : null;
    }

    public function updateAddress(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity
    {
        $result = $this->addressRepository->update($id, $attributes, $withoutLoadingModel);
        return $result ? (new AddressEntity)->fill($result) : null;
    }

    public function deleteAddress(int|array $ids, bool $forceDelete = false): void
    {
        $this->addressRepository->delete($ids, $forceDelete);
    }
}
