<?php

namespace D3JDigital\Addresses\Filters;

class AddressFilter
{
    const GET_ADDRESS_TYPES = 'plugin.addresses.filter.get-address-types';
    const GET_COUNTRIES = 'plugin.addresses.filter.get-countries';
    const PREPARE_ADDRESS_RESOURCE = 'plugin.addresses.filter.prepare-address-resource';
}
