<?php

namespace D3JDigital\Addresses\Enums;

enum AddressType
{
    case PRIMARY;
    case SECONDARY;
    case BILLING;
    case SHIPPING;
}
