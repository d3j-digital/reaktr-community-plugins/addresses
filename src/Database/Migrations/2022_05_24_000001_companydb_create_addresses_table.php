<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CompanydbCreateAddressesTable extends Migration
{
    public function up(): void
    {
        if (!Schema::hasTable('addresses')) {
            Schema::create('addresses', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('linked_resource_id')->nullable();
                $table->string('linked_resource_type')->nullable();
                $table->string('type');
                $table->string('company')->nullable();
                $table->string('building_name')->nullable();
                $table->string('line_one')->nullable();
                $table->string('line_two')->nullable();
                $table->string('city')->nullable();
                $table->string('region')->nullable();
                $table->string('postcode')->nullable();
                $table->string('country')->nullable();
                $table->decimal('latitude', 10, 8)->nullable();
                $table->decimal('longitude', 10, 8)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(['linked_resource_id', 'linked_resource_type'], 'addressable');
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('addresses');
    }
}
