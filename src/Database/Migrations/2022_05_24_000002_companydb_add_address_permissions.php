<?php

use Illuminate\Database\Migrations\Migration;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\AuthService;
use OpenSourceDeveloper\Reaktr\Core\Base\PermissionObject;

class CompanydbAddAddressPermissions extends Migration
{
    protected AuthService $authService;

    public function __construct()
    {
        $this->authService = app(AuthService::class);
    }

    public function up(): void
    {
        $permissions = [
            new PermissionObject('addresses.*', 'reaktr.permissions.global.all_actions', ['resource' => 'plugin-addresses.resources.address.name.singular']),
            new PermissionObject('addresses.index', 'reaktr.permissions.global.list', ['resource' => 'plugin-addresses.resources.address.name.singular']),
            new PermissionObject('addresses.store', 'reaktr.permissions.global.create', ['resource' => 'plugin-addresses.resources.address.name.singular']),
            new PermissionObject('addresses.show', 'reaktr.permissions.global.view', ['resource' => 'plugin-addresses.resources.address.name.singular']),
            new PermissionObject('addresses.update', 'reaktr.permissions.global.update', ['resource' => 'plugin-addresses.resources.address.name.singular']),
            new PermissionObject('addresses.destroy', 'reaktr.permissions.global.delete', ['resource' => 'plugin-addresses.resources.address.name.singular']),
        ];

        foreach ($permissions as $permission) {
            $this->authService->createPermission($permission);
        }
    }
}
