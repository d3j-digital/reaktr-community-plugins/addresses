<?php

namespace D3JDigital\Addresses\Database\Eloquent\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Repository;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\SystemModel;
use D3JDigital\Addresses\Contracts\Repositories\AddressRepositoryInterface;
use D3JDigital\Addresses\Database\Eloquent\Models\Address;

class AddressRepository extends Repository implements AddressRepositoryInterface
{
    /**
     * @return Model|SystemModel
     */
    function model(): Model|SystemModel
    {
        return new Address();
    }
}
