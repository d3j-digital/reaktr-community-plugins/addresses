<?php

namespace D3JDigital\Addresses\Database\Eloquent\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;

class Address extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'addresses';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return MorphTo
     */
    public function addressable(): MorphTo
    {
        return $this->morphTo();
    }
}
