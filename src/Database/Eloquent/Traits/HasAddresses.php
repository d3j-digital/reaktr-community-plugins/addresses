<?php

namespace D3JDigital\Addresses\Database\Eloquent\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use D3JDigital\Addresses\Database\Eloquent\Models\Address;

class HasAddresses
{
    /**
     * @return MorphMany
     */
    public function addresses(): MorphMany
    {
        return $this->morphMany(Address::class, 'linked_resource');
    }
}
