<?php

namespace D3JDigital\Addresses\Request\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use D3JDigital\Addresses\Response\Entities\AddressEntity;

class StoreAddress extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => ['required', Rule::in(AddressEntity::getAvailableTypes())],
            'company' => 'sometimes|string',
            'building_name' => 'sometimes|string',
            'line_one' => 'required',
            'line_two' => 'sometimes',
            'city' => 'required|string',
            'region' => 'required|string',
            'postcode' => 'required|string',
            'country' => ['required', 'min:2', 'max:2'],
            'latitude' => 'required|string',
            'longitude' => 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'this field is required',
            'type.in' => 'you can only specify one of the following accepted types (' . implode(',', AddressEntity::getAvailableTypes()) . ')',
            'min' => 'this field must contain a minimum of :min characters',
            'max' => 'this field can only contain a maximum of :max characters',
        ];
    }
}
