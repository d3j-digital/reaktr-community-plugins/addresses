<?php

namespace D3JDigital\Addresses\Request\Controllers\Api;

use Illuminate\Http\JsonResponse;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Controller;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Addresses\Contracts\Services\AddressServiceInterface;
use D3JDigital\Addresses\Request\Validation\StoreAddress;
use D3JDigital\Addresses\Request\Validation\UpdateAddress;
use D3JDigital\Addresses\Response\Resources\AddressResource;

class AddressController extends Controller
{
    /**
     * @param RoutingService $routingService
     * @param AddressServiceInterface $addressService
     */
    public function __construct(
        protected RoutingService $routingService,
        protected AddressServiceInterface $addressService,
    ) {
        parent::__construct();
        $this->middleware('UserIsAuthenticated:jwt');
        $this->middleware('UserIsAuthorised:addresses.index')->only('index');
        $this->middleware('UserIsAuthorised:addresses.show')->only('show');
        $this->middleware('UserIsAuthorised:addresses.store')->only('store');
        $this->middleware('UserIsAuthorised:addresses.update')->only('update');
        $this->middleware('UserIsAuthorised:addresses.destroy')->only('destroy');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(200, '', AddressResource::collection($this->addressService->getAddresses($this->relations, $this->columns)));
    }

    /**
     * @param StoreAddress $request
     * @return JsonResponse
     */
    public function store(StoreAddress $request): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(201, '', new AddressResource($this->addressService->createAddress($request->validated())));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        if ($resource = $this->addressService->getAddress($id, $this->relations, $this->columns)) {
            return $this->routingService->renderJsonSuccessResponse(200, '', new AddressResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(404);
    }

    /**
     * @param UpdateAddress $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateAddress $request, int $id): JsonResponse
    {
        if ($resource = $this->addressService->updateAddress($id, $request->validated())) {
            return $this->routingService->renderJsonSuccessResponse(202, '', new AddressResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(204);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->addressService->deleteAddress($id);
        return $this->routingService->renderJsonSuccessResponse(204);
    }
}
