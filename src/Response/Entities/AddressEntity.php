<?php

namespace D3JDigital\Addresses\Response\Entities;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use D3JDigital\Addresses\Filters\AddressFilter;
use D3JDigital\Addresses\Enums\AddressType;
use D3JDigital\Addresses\Enums\Country;

class AddressEntity extends Entity
{
    public ?int $id = null;
    public ?int $linkedResourceId = null;
    public ?string $linkedResourceType = null;
    public ?string $type = null;
    public ?string $company = null;
    public ?string $buildingName = null;
    public ?string $lineOne = null;
    public ?string $lineTwo = null;
    public ?string $city = null;
    public ?string $region = null;
    public ?string $postcode = null;
    public ?string $country = null;
    public ?float $latitude = null;
    public ?float $longitude = null;

    /**
     * @return array
     */
    public static function getAvailableTypes(): array
    {
        return app()->triggerFilter(AddressFilter::GET_ADDRESS_TYPES, collect(AddressType::cases())->pluck('name')->toArray());
    }

    /**
     * @return array
     */
    public function getAvailableCountries(): array
    {
        return app()->triggerFilter(AddressFilter::GET_COUNTRIES, collect(Country::cases())->pluck('name')->toArray());
    }
}
