<?php

namespace D3JDigital\Addresses\Response\Resources;

use OpenSourceDeveloper\Reaktr\Core\Response\Resources\JsonResource;
use D3JDigital\Addresses\Filters\AddressFilter;
use D3JDigital\Addresses\Response\Entities\AddressEntity;

/**
 * @mixin AddressEntity
 */
class AddressResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => 'address',
            'attributes' => [
                'id' => $this->id,
                'linked_resource_id' => $this->linkedResourceId,
                'linked_resource_type' => $this->linkedResourceType,
                'type' => $this->type,
                'company' => $this->company,
                'building_name' => $this->buildingName,
                'line_one' => $this->lineOne,
                'line_two' => $this->lineTwo,
                'city' => $this->city,
                'region' => $this->region,
                'postcode' => $this->postcode,
                'country' => $this->country,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'created_at' => $this->createdAt,
                'updated_at' => $this->updatedAt,
                'deleted_at' => $this->deletedAt,
            ],
        ];

        return app()->triggerFilter(AddressFilter::PREPARE_ADDRESS_RESOURCE, $data, $this);
    }
}
