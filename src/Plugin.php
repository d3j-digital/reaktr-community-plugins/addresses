<?php

namespace D3JDigital\Addresses;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Addresses\Contracts\Repositories\AddressRepositoryInterface;
use D3JDigital\Addresses\Contracts\Services\AddressServiceInterface;
use D3JDigital\Addresses\Database\Eloquent\Repositories\AddressRepository;
use D3JDigital\Addresses\Services\AddressService;

class Plugin extends ReaktrPlugin
{
    public ?string $pluginName = 'Addresses';
    public ?string $pluginKey = 'addresses';
    public ?string $pluginVendorName = 'D3JDigital';
    public ?string $pluginVendorKey = 'd3j-digital';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'Adds address functionality';
    public ?array $pluginDependencies = [];

    public array $singletons = [
        AddressRepositoryInterface::class => AddressRepository::class,
        AddressServiceInterface::class => AddressService::class,
    ];

    public function registerRoutes(): void
    {
        $routingService = app(RoutingService::class);
        $routingService->registerApiRoutes('D3JDigital\Addresses\Request\Controllers\Api', __DIR__ . '/Request/Routes/Api/v1.php', 1);
    }

    public function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }
}
